<%--
  ~ Copyright (c) 2020, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
  ~
  ~ WSO2 Inc. licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  --%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.wso2.carbon.identity.mgt.endpoint.util.IdentityManagementEndpointUtil" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.Constants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.TenantDataManager" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.AuthenticationEndpointUtil" %>
<%@ include file="includes/localize.jsp" %>


<html>
<head>
    <!-- header -->
    <%
        File headerFile = new File(getServletContext().getRealPath("extensions/header.jsp"));
        if (headerFile.exists()) {
    %>
    <jsp:include page="extensions/header.jsp"/>
    <% } else { %>
    <jsp:include page="includes/header.jsp"/>
    <% } %>
    <script src="js/scripts.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-portal layout x509-certificate-portal-layout">
<main class="center-segment">
    <div class="ui container medium center aligned middle aligned">
        <!-- product-title -->
        <%
            File productTitleFile = new File(getServletContext().getRealPath("extensions/product-title.jsp"));
            if (productTitleFile.exists()) {
        %>
        <jsp:include page="extensions/product-title.jsp"/>
        <% } else { %>
        <jsp:include page="includes/product-title.jsp"/>
        <% } %>

        <div class="align-right buttons">
            <input type="text" id="linkTo" name="lname">
            <input type="button" class="ui primary large button"
                   onclick="submitIdentifier();"
                   value="ok boy call local"/>
        </div>
        <div class="ui segment">
            <!-- page content -->
            <table id="tableData">
                <tr>
                    <th>SubjectDN</th>
                    <th>Action</th>
                </tr>

            </table>
        </div>
    </div>
    <script type="text/javascript">

        let urlData = window.location.href;
        let linkAndParam = urlData.split("&");
        let paramLinkTo = decodeURIComponent(decodeURIComponent(linkAndParam[linkAndParam.length - 1]));
        console.log("paramLinkTo : " + paramLinkTo);
        let dataCallAjax = null;

        function submitIdentifier() {
            $.ajax({
                type: "GET",
                url: "http://127.0.0.1:14423/api/v1/certs",
                success: function (responseBody) {
                    console.log("data get local : " + responseBody);
                    const tb = document.getElementById("tableData");
                    dataCallAjax = responseBody.data;
                    for (let i = 0; i < dataCallAjax.length; i++) {
                        const nodeTr = document.createElement("tr");
                        const nodeTd1 = document.createElement("td");
                        const nodeTd2 = document.createElement("td");
                        //const nodeInput = document.createElement("button");

                        var xmlString = ('<button onclick="gotoSite(' + i + ')">Click me</button>')
                        // var button = new DOMParser().parseFromString(xmlString, "text/xml");
                        const textnodeTd1 = document.createTextNode(dataCallAjax[i].subjectDn);

                        nodeTd1.appendChild(textnodeTd1);
                        nodeTd2.innerHTML = xmlString;
                        nodeTr.appendChild(nodeTd1);
                        nodeTr.appendChild(nodeTd2);
                        tb.appendChild(nodeTr);
                    }
                },
                cache: false
            });
        }

        function gotoSite(index) {
            let tmp = dataCallAjax[index];
            let urlTmp = (paramLinkTo.split("=")[1] + "?"+urlData.split("?")[1]) + "&certificate="+tmp.certificate;
            debugger;
            console.log("urlTmp: " + urlTmp);
            console.log("aaaa " + tmp.certificate);
            window.location = urlTmp;
            // $.ajax({
            //     type: "GET",
            //     url: urlTmp,
            //     headers: {
            //         "certificate": tmp.certificate,
            //     },
            //     success: function (responseBody) {
            //         console.log("bbbbb" + responseBody);
            //     },
            //     cache: false
            // });
        }
    </script>
</main>

<!-- product-footer -->
<%
    File productFooterFile = new File(getServletContext().getRealPath("extensions/product-footer.jsp"));
    if (productFooterFile.exists()) {
%>
<jsp:include page="extensions/product-footer.jsp"/>
<% } else { %>
<jsp:include page="includes/product-footer.jsp"/>
<% } %>

<!-- footer -->
<%
    File footerFile = new File(getServletContext().getRealPath("extensions/footer.jsp"));
    if (footerFile.exists()) {
%>
<jsp:include page="extensions/footer.jsp"/>
<% } else { %>
<jsp:include page="includes/footer.jsp"/>
<% } %>
</body>
</html>

